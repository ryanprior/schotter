#!/usr/bin/env python3

from math import sqrt, pi, sin, cos

def byte_to_braille(byte):
    code = 0x2800 + byte
    output = (0xE0 | (code >> 12)) + (0x80 | ((code >> 6) & 0x3F)) + (0x80 | (code & 0x3F))
    return output.to_bytes(3, byteorder='little').decode('utf-8')

print(byte_to_braille(0x0000))
print(byte_to_braille(0x00ff))

class Canvas(list):
    def __init__(self, x, y):
        """
        creates a list of lists of zeros
        """
        super(Canvas, self).__init__([[0 for _ in range(0, x)] for _ in range(0, y)])
    def __repr__(self):
        result = ""
        for row in self:
            for column in row:
                result += str(column)
            result += '\n'
        return result
    def drawLine(self, pointA, pointB):
        """
        pointA and pointB are tuples of integer (eg. `(1, 5)`)

        this method mutates self to draw the line
        """
        xA, yA = pointA
        xB, yB = pointB
        dx = abs(xB-xA)
        dy = abs(yB-yA)
        signX = 1 if xA < xB else -1
        signY = 1 if yA < yB else -1

        error = int(dx-dy)
        error2 = 0

        x = int(xA)
        y = int(yA)
        while True:
            self[y][x]=1
            if x == xB and y == yB:
                break
            error2 = error * 2
            if error2 > -dy:
                error -= dy
                x += signX
            if error2 < dx:
                error += dx
                y += signY
    def drawSquare(self, center, size, angle):
        """
        center is an integer tuple point, eg `(1, 5)`
        size is in units relative to the unit circle
        angle is in radians
        """
        size = round(size / sqrt(2), 0)
        k = pi/4 + angle
        x, y = center
        angles = [k, k+pi/2, k+pi, k+3*pi/2]
        points = tuple((round(sin(k)) * size + x,
                        round(cos(k)) * size + y) for k in angles)

        for i in range(0, 4):
            self.drawLine(points[i], points[(i+1)%4])

canvas = Canvas(10,10)
canvas.drawSquare((5,5), 3, pi/2)
print(canvas)
